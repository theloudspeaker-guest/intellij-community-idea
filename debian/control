Source: intellij-community-idea
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Saif Abdul Cassim <saif.15@cse.mrt.ac.lk>
Build-Depends:
 ant,
 debhelper-compat (= 12),
 dh-exec,
 gradle-debian-helper,
 libautomaton-java,
 libbatik-java,
 libcommons-codec-java,
 libcommons-compress-java,
 libguava-java,
 libhttpclient-java,
 libimgscalr-java,
 libini4j-java,
 libintellij-java-compatibility-java,
 libjdom2-intellij-java,
 libjetbrains-annotations-java,
 libjgoodies-forms-java,
 libjgraph-java,
 libjna-platform-java,
 liblog4j1.2-java,
 liblz4-java,
 libmicroba-java,
 libnanoxml2-java,
 libnetty-java,
 liboro-java (>= 2.0.8a-14~),
 libpicocontainer1-java,
 libstreamex-java,
 libtrove-intellij-java,
 libxml-commons-external-java,
 libxmlgraphics-commons-java,
 maven-repo-helper,
 openjdk-11-jdk-headless,
 openjdk-8-jdk-headless
Standards-Version: 4.4.0
Vcs-Browser: https://salsa.debian.org/java-team/intellij-community-idea
Vcs-Git: https://salsa.debian.org/java-team/intellij-community-idea.git
Homepage: https://github.com/JetBrains/intellij-community

Package: libintellij-utils-java
Architecture: all
Depends:
 ${misc:Depends},
 libbatik-java,
 libcommons-compress-java,
 libimgscalr-java,
 libintellij-java-compatibility-java,
 libjdom2-intellij-java,
 libjetbrains-annotations-java,
 libjna-platform-java,
 liblog4j1.2-java,
 liblz4-java,
 liboro-java,
 libtrove-intellij-java,
 libxml-commons-external-java,
 libxmlgraphics-commons-java
Description: IntelliJ IDEA utility classes
 IntelliJ IDEA is a Java integrated development environment for
 developing computer software. Community edition is an Apache 2
 licensed version of IDEA.
 .
 This package ships utility classes used by IntelliJ IDEA and
 other software.

Package: libintellij-jps-model-java
Architecture: all
Depends:
 ${misc:Depends},
 libintellij-utils-java,
 libjdom2-intellij-java,
 libjetbrains-annotations-java
Description: IntelliJ IDEA Project Model
 IntelliJ IDEA Community Edition is a Java integrated development environment
 (IDE) for developing computer software.
 .
 This package ships parts of the build system of IntelliJ IDEA
 used by IDEA and other software to handle IDEA projects files.

Package: libintellij-extensions-java
Architecture: all
Depends:
 ${misc:Depends},
 libintellij-utils-java,
 libjdom2-intellij-java,
 libpicocontainer1-java
Description: IntelliJ IDEA Extension Point Framework
 IntelliJ IDEA Community Edition is a Java integrated development environment
 (IDE) for developing computer software.
 .
 Provides interfaces for the extension point framework used in IDEA
 and other software.

Package: libintellij-platform-api-java
Architecture: all
Depends: ${misc:Depends}
Description: IntelliJ IDEA Platform API
 IntelliJ IDEA Community Edition is a Java integrated development environment
 (IDE) for developing computer software.
 .
 This package contains the platform:platform-api artifacts.

Package: libintellij-platform-impl-java
Architecture: all
Depends: ${misc:Depends}
Description: IntelliJ IDEA Platform Implementation
 IntelliJ IDEA Community Edition is a Java integrated development environment
 (IDE) for developing computer software.
 .
 This package contains the platform:core-impl artifact.

Package: libintellij-core-java
Architecture: all
Depends: ${misc:Depends}
Description: IntelliJ IDEA Core API
 IntelliJ IDEA Community Edition is a Java integrated development environment
 (IDE) for developing computer software.
 .
 This package contains the platform:core-impl, platform:core-api, platform:util
 and platform:util-rt artifacts.
